// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
(function(){
    var app = angular.module('starter', ['ionic', 'uiRouterStyles', 'ngCordova']);

    app.run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if(window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    });
    app.controller('appCtrl', ['$scope', function($scope){
        $scope.platform = ionic.Platform;
    }]);
    app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
        $stateProvider.state('home', {
            url: '/home',
            templateUrl: 'views/home.html',
            controller: 'homeCtrl',
            data: {
                //css: 'components/home/css/home.css'
            }
        });
        $stateProvider.state('servicios', {
            url: '/servicios',
            templateUrl: 'views/servicios.html',
            controller: 'serviciosCtrl',
            data: {
                //css: 'components/servicios/css/servicios.css'
            }
        });
        $stateProvider.state('solicitar', {
            url: '/solicitar',
            templateUrl: 'views/solicitar.html',
            controller: 'solicitarCtrl',
            params: {servicio: null},
            data: {
                //css: 'components/solicitar/css/solicitar.css'
            }
        });
        $stateProvider.state('aviso', {
            url: '/aviso',
            templateUrl: 'views/aviso.html',
            controller: 'avisoCtrl',
            params: {data: null},
            data: {
                //css: 'components/aviso/css/aviso.css'
            }
        });
        $stateProvider.state('ofrecer', {
            url: '/ofrecer',
            templateUrl: 'views/ofrecer.html',
            controller: 'ofrecerCtrl',
            data: {
                //css: 'components/ofrecer/css/ofrecer.css'
            }
        });
        $urlRouterProvider.otherwise('/home');
    }]);
}());
