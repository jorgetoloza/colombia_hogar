angular.module('starter').controller('serviciosCtrl', ['$scope', '$http', '$state', function($scope, $http, $state){
	$scope.back = function($event){
		if($($event.currentTarget).hasClass('show')){
			$state.go('home');
		}
	}
	$scope.goSolicitar = function($event){
		$state.go('solicitar', {servicio: $event.currentTarget.attributes['data-name'].value});
	}
}]);
