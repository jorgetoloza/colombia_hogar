angular.module('starter').controller('solicitarCtrl', ['$scope', '$http', '$state', '$stateParams',  function($scope, $http, $state, $stateParams){
	$scope.data = {};
	$scope.sending = false;
	if($stateParams.servicio)
		$scope.data.servicio = $stateParams.servicio;
	$scope.solicitar = function($event){
		// peticion al api
		$scope.solicitarForm.$setSubmitted();
		if(!$scope.sending && $scope.solicitarForm.$valid){
			$scope.sending = true;
			$http({
			    method: 'POST',
			    url: 'https://colombia-hogar.herokuapp.com/server.php',
			    data: {
					tipo: 'solicitud', 
					nombre: $scope.data.nombre, 
					apellidos: $scope.data.apellidos,
					celular: $scope.data.celular,
					ciudad: $scope.data.ciudad,
					servicio: $scope.data.servicio
				},
			    headers: {
			    	'Content-Type': 'application/json'
			    }
			}).then(
				function (response) {
		            console.log(response);
		            $scope.sending = false;
		            if(response.data == 'OK'){
						$state.go('aviso', 
							{
								data:{
									titulo: 'Listo',
									mensaje: 'Gracias por utilizar nuestros servicios, en un momento nos pondremos en contacto contigo.',
									error: false,
									servicio: $scope.data.servicio
								}
							}
						);
		            }
		        }, function (e) {
	            	console.error(e);
	            	$scope.sending = false;
	            	$state.go('aviso', 
						{
							data:{
								titulo: 'Parece que no tienes internet',
								mensaje: 'Debes estar conectado a internet para que podamos recibir tus datos.',
								servicio: $scope.data.servicio,
								error: true
							}
						}
					);
	        	}
	        );
		}
	}
	
	$scope.back = function($event){
		if($($event.currentTarget).hasClass('show')){
			$state.go('servicios');
		}
	}
}]);
