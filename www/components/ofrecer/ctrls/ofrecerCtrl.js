angular.module('starter').controller('ofrecerCtrl', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state){
	$scope.data = {};
	$scope.sending = false;
	$scope.ofrecer = function($event){
		// peticion al api
		$scope.ofrecerForm.$setSubmitted();
		if(!$scope.sending && $scope.ofrecerForm.$valid){
			$scope.sending = true;
	        $http({
			    method: 'POST',
			    url: 'https://colombia-hogar.herokuapp.com/server.php',
			    data: JSON.stringify({
					tipo: 'ofrecimiento', 
					nombre: $scope.data.nombre, 
					apellidos: $scope.data.apellidos,
					celular: $scope.data.celular,
					ciudad: $scope.data.ciudad,
					correo: $scope.data.correo,
					genero: $scope.data.genero,
					experiencia: $scope.data.experiencia,
					servicio: $scope.data.servicio
				})
			}).then(
				function (response) {
		            console.log(response);
					$scope.sending = false;

		            if(response.data == 'OK'){
						$state.go('aviso', 
							{
								data:{
									titulo: 'Ya estas registrado',
									mensaje: 'No olvides que una vez registrados tus datos empieza el proceso de verificacion.',
									error: false
								}
							}
						);
		            }
		        }, function (e) {
	            	console.error(e);
					$scope.sending = false;

	            	$state.go('aviso', 
						{
							data:{
								titulo: 'Parece que no tienes internet',
								mensaje: 'Debes estar conectado a internet para que podamos recibir tus datos.',
								error: true
							}
						}
					);
	        	}
	        );
		}
	}
	$scope.back = function($event){
		if($($event.currentTarget).hasClass('show')){
			$state.go('home');
		}
	}
}]);
