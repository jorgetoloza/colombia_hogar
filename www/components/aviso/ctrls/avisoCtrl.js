angular.module('starter').controller('avisoCtrl', ['$scope', '$state', '$stateParams',  function($scope, $state, $stateParams){
	$scope.data = {};
	if($stateParams.data){
		console.log($stateParams.data);
		$scope.data.mensaje = $stateParams.data.mensaje;
		$scope.data.titulo = $stateParams.data.titulo;
		$scope.data.error = $stateParams.data.error;
		$scope.data.servicio = $stateParams.data.servicio;
	}
	
	$scope.back = function($event){
		if($($event.currentTarget).hasClass('show')){
			if($scope.data.error){
				if($scope.data.servicio)
					$state.go('solicitar', {servicio: $scope.data.servicio});
				else
					$state.go('ofrecer');
			}else{
				$state.go('home');
			}
		}
	}
}]);
