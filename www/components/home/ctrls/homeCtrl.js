angular.module('starter').controller('homeCtrl', ['$scope', '$http', '$state', function($scope, $http, $state){
	$scope.goServicios = function($event){
		$state.go('servicios');
	}
	$scope.goOfrecer = function($event){
		$state.go('ofrecer');
	}
	if(window.localStorage.getItem("firstTime") == undefined || window.localStorage.getItem("firstTime") == true){
		$scope.firstTime = true;
		window.localStorage.setItem("firstTime", true);
	}else{
		$scope.firstTime = false;
		window.localStorage.setItem("firstTime", false);
	}
	$scope.setFirstTime = function(value){
		$scope.firstTime = value;
		window.localStorage.setItem("firstTime", value);
	}
	$scope.toggleAbout = function(){
		var about = document.querySelector('.vista[name="home"] #about');
		if(angular.element(about).hasClass('show')){
			angular.element(about).children().css({opacity: 0});
			setTimeout(function(){
				angular.element(about).removeClass('show');
			}, 510);
		}else{
			angular.element(about).addClass('show');
			setTimeout(function(){
				angular.element(about).children().css({opacity: 1});
			}, 510);
		}
	}
}]);

